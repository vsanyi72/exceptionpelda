package vs.exception;

import java.util.Scanner;

public class PozitivBeolvas 
{
	private Scanner sc;
	
	public PozitivBeolvas()
	{
		this.sc = new Scanner(System.in);
	}
	/**
	 * 
	 * @return
	 * @throws SajatException
	 */
	public Integer readPosInt() throws NotPositiveException
	{
		int szam = this.sc.nextInt();
		if(szam<0)
		{
			throw new NotPositiveException("Nem lehet negat�v sz�mot beolvasni");
		}
		return szam;
	}
}

