package vs.exception.pelda;

import java.util.Scanner;

public class Start2 {

	public static void main(String[] args) {
		// Bek�r�nk 2 sz�mot
		// az els�t elosztjuk a m�sodikkal
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Adj meg egy sz�mot");
		double a = sc.nextDouble();
		System.out.println("Adj meg egy sz�mot");
		double b = sc.nextDouble();
		double eredmeny=0;
		try {
			eredmeny = osztas( a,b) ;
			System.out.println(eredmeny);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			//e.printStackTrace();
		}
		
	}

	private static double osztas(double a , double b) throws Exception 
	{
		if (b==0) 
		{
			throw new Exception("Nem lehet null�val osztani");
		}
		
		return a/b; 
		
	}
}
